<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tasks';
$this->params['breadcrumbs'][] = $this->title;
?>
         <?php    if (\Yii::$app->user->can('manageUsers')) {?>
<div class="task-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
     <?php } ?>
    <p>
        <?= Html::a('Create Task', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            //'urgency',
            'created_at',
            'updated_at',
            'crated_by',
            'updated_by',
            ['attribute'=> 'urgency name','value'=> 'urgency.urgency'],
           // ['attribute'=> 'user name','value'=> 'user.name'],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
