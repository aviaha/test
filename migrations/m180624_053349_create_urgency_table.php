<?php

use yii\db\Migration;

/**
 * Handles the creation of table `urgency`.
 */
class m180624_053349_create_urgency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('urgency', [
            'id' => $this->primaryKey(),
             'urgency' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('urgency');
    }
}
