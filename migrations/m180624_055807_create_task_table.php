<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task`.
 */
class m180624_055807_create_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('task', [
           
            'id' => $this->primaryKey(),
                'name'=> $this->string(),
                'urgency' => $this->string(),
                /*'username' => $this->string()->unique(),
                'auth_key' => $this->string(), 
                'password' => $this->string(),*/
                'created_at' => $this->timestamp(),
                'updated_at' => $this->timestamp(),
                'crated_by' => $this->integer(),              
                'updated_by' => $this->integer(),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('task');
    }
}
