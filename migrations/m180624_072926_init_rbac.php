<?php

use yii\db\Migration;
use yii\rbac\Rule;
use app\models\User;
use app\rbac\EmployeeRule;


/**
 * Class m180624_072926_init_rbac
 */
class m180624_072926_init_rbac extends Migration
{
   
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
 $auth = Yii::$app->authManager;

 ///////////הגדרת התפקידים /////////////

    $manager = $auth->createRole('manager');
      $auth->add($manager);
              
      $employee = $auth->createRole('employee');
      $auth->add($employee);;

////////////////הגדרת ירושות///////////
      $auth->addChild($manager, $employee);

 //////////////// הגדרת ההרשאות //////////////////
      $manageUsers = $auth->createPermission('manageUsers');//למנהל
      $auth->add($manageUsers);

     $manageTask = $auth->createPermission('manageTask');//לעובד
      $auth->add($manageTask); 

      $updateOwnuser = $auth->createPermission('updateOwnuser'); ///עריכת המשתמש שלו בעצמוצ- תנאי מורכב יותר 
      $rule = new \app\rbac\EmployeeRule;
      $auth->add($rule);
      $updateOwnuser->ruleName = $rule->name;                
      $auth->add($updateOwnuser);                 
        

//////////////////////קביעה מי יכול ליישם איזה חוק
      $auth->addChild($manager, $manageUsers);
      $auth->addChild($employee, $manageTask);
      $auth->addChild($updateOwnuser, $manageTask);   

/*////////////////////// הגדרת המשתמשים הרשומים המערכת ()
$auth->assign($admin,1);
$auth->assign($teamleader,2);
$auth->assign($member,3);
$auth->assign($manager,4);
$auth->assign($admin,5);*/
    }
    
   }